## Hvordan få tak i oppgavene?

* Åpne et terminal vindu
* Naviger til mappen du ønsker å lagre prosjektet
* skriv: "git clone https://gitlab.stud.idi.ntnu.no/itgk2021/viderekommen-ovingsforelesninger.git"
* Åpne VSCode, trykk file -> open folder og velg mappen du nettopp klonet
* Åpne mappen for valgt øvingsforelesning
